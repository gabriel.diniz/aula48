public class aula48 { // manipulando arrays

    public static void main(String[] args) {

        int[] arrayInt = new int[10]; // array inteiro, nova instancia tipo int 10 elementos

        /*
        arrayInt[0] = 10; // array posição 0 valor 10
        arrayInt[1] = 20; // array posição 1 valor 209
        arrayInt[2] = 30; // array posição 2 valor 30
         */

        arrayInt[0] = 10; // array posição 0 valor 10
        arrayInt[1] = 20; // array posição 1 valor 20
        arrayInt[2] = 30; // array posição 2 valor 30
        arrayInt[3] = 40; // array posição 3 valor 40
        arrayInt[4] = 50; // array posição 4 valor 50
        arrayInt[5] = 60; // array posição 5 valor 60
        arrayInt[6] = 70; // array posição 6 valor 70
        arrayInt[7] = 80; // array posição 7 valor 80
        arrayInt[8] = 90; // array posição 8 valor 90
        arrayInt[9] = 100; // array posição 9 valor 100



        int soma = arrayInt[0] + arrayInt[1] + arrayInt[2];  // variavel soma, soma de arrays

        System.out.println("A soma dos primeiros elementos é: "+soma); // imprime a soma

        int soma2 = 0;  // variavel
        for (int i=0; i<10; i++){ /// variavel inteira nome i, valor 0, executar enquanto i menor que 10 , acrecenta 1
            soma2 += arrayInt[i]; // soma 2 igual arrayint
        }

        System.out.println("A soma de todos elementos é? "+ soma2); // imprime soma 2

    }
}
